##1. What Is ECMAScript 6 (ES6)?

- 1995: JavaScript created

- 1997: ECMAScript 1

- 2009: ECMAScript 5

- 2015: ECMAScript 6

- Babel is a JavaScript compiler, converts ES6 to ES5, used in CoffeeScript and TypeScript

##2. Transpiling ES6

- Babel:
	
	+ ES6 code in -> ES5 code out
	
	+ Created by Sebastian McKenzie
	
	+ Used frequently with React

- Use webpack to automate the transpiling process

	+ Webpack is a build tool that help us load all of our dependencies: CSS, images, node modules and more
	
-  The babel package is no more. Previously, it was the entire compiler and all the transforms plus a bunch of CLI tools, but this lead to unnecessarily large downloads and was a bit confusing. Now it's split up into two separate packages: babel-cli and babel-core.

##3. ES6 Syntax

- let

- const

- Template strings:

	+ Format JS code (string) with variables
	
	+ Can make longer, better strings with variables and custom formatting (dynamic content)

- ...: An array within an array

##4. ES6 Functions & Objects

- Arrow functions:

	+ =>: Make the code more compact and readable, and help deal with the scope of this

	+ Reference: https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/

- Generators:

	+ Allow us to pause functions
	
	+ Pause with yield

	+ Get next yield with .next()

	+ Used when we're doing with some sort asynchronous event

##5. ES6 Classes:

- React.js is an popular JavaScript library that helps build user interfaces.



	
